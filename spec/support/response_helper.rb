# frozen_string_literal: true

module ResponseHelper
  class DecoratedResponse
    extend Forwardable

    def_delegators :@response,
                   :body, :status, :headers, :content_type, :successful?

    def initialize(response)
      @response = response
    end

    def json
      @_json ||= JSON.parse(body, symbolize_names: true)
    end

    def json?
      content_type == 'application/json'
    end

    def body_with
      yield json
    end

    def as_object
      OpenStruct.new(json)
    end
  end

  def response
    @_response ||= DecoratedResponse.new(last_response)
  end
end

RSpec.configure do |config|
  config.include ResponseHelper, type: :api
end
