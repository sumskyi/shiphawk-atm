# frozen_string_literal: true

require 'spec_helper'

describe Banknote do
  describe 'validations' do
    subject(:banknote) { Banknote.new(options) }
    let(:options) do
      { denomination: denomination, count: count }
    end

    context 'denomination' do
      let(:count) { 0 }

      [1, 2, 5, 10, 25, 50].each do |denomination|
        context denomination do
          let(:denomination) { denomination }

          it 'is valid' do
            expect(banknote).to be_valid
          end
        end
      end

      [3, 4, 11].each do |denomination|
        context denomination do
          let(:denomination) { denomination }

          it 'is invalid' do
            expect(banknote).to_not be_valid
          end
        end
      end
    end
    # denomination

    context 'count' do
      let(:denomination) { 1 }

      context 'integers' do
        context 'positive' do
          let(:count) { 1 }
          specify { is_expected.to be_valid }
        end

        context 'negative' do
          let(:count) { -1 }
          specify { is_expected.to_not be_valid }
        end
      end

      context 'arrays' do
        let(:count) { [] }
        specify { is_expected.to_not be_valid }
      end
    end
    # count
  end
end
