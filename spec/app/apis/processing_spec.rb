# frozen_string_literal: true

require 'spec_helper'

describe ATM::Processing, type: :api do
  include Rack::Test::Methods

  context 'charge' do
    it 'successfull' do
      post '/charge', denomination: { '5' => 2, '50' => 3 }
      expect(response).to be_successful
    end
  end

  context 'take_out' do
    it 'successfull' do
      post '/take_out', amount: 50
      expect(response).to be_successful
    end
  end
end
