# frozen_string_literal: true

describe ChargeInteraction do
  let(:params) do
    {
      denomination: {
        '5' => '3', '50' => 4
      }
    }
  end

  subject { described_class.run(params) }

  it 'records new banknotes' do
    expect { subject }.to change { Banknote.count }.by(2)
  end

  it 'updates existing records' do
    note = Banknote.create!(denomination: 5, count: 11)
    subject
    expect(note.reload.count).to eq 14
  end
end
