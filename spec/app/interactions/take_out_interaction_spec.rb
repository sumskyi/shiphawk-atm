# frozen_string_literal: true

describe TakeOutInteraction do
  subject { described_class.run(params) }

  context 'enough funds' do
    # 50 * 3 + 25 * 2 + 5 * 10 = 250
    before do
      Banknote.create!(denomination: 50, count: 3)
      Banknote.create!(denomination: 25, count: 2)
      Banknote.create!(denomination: 5, count: 10)
    end

    context 'takes out funds' do
      let(:params) do
        { amount: 210 }
      end

      it 'takes out funds' do
        subject

        expect(Banknote.total).to eq 40
      end
    end

    context 'no such notes' do
      let(:params) do
        { amount: 2 }
      end

      it 'cannot takes out funds' do
        subject
        expect(subject.result).to eq(error: 'Sorry, no such banknotes')

        expect(Banknote.total).to eq 250
      end
    end
  end

  context 'not enough funds' do
    let(:params) do
      { amount: 50 }
    end

    it 'cannot take out funds' do
      expect(subject.result).to eq(error: 'Insuficcient funds')
    end
  end
end
