# frozen_string_literal: true

ENV['RACK_ENV'] ||= 'development'

require 'active_interaction'
require 'active_record'
require 'grape'
require 'yaml'

%w[apis models interactions].each do |path|
  Dir[File.dirname(__FILE__) + "/../app/#{path}/**/*.rb"].each do |file|
    require file
  end
end

db = YAML.load_file('config/database.yml')
ActiveRecord::Base.establish_connection(db[ENV['RACK_ENV']])
