class CreateBanknotes < ActiveRecord::Migration[5.0]
  def change
    create_table :banknotes do |t|
      t.integer :denomination
      t.integer :count, default: 0
    end
  end
end
