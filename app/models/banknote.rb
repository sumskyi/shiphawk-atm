# frozen_string_literal: true

class Banknote < ActiveRecord::Base
  validates :denomination, inclusion: { in: [1, 2, 5, 10, 25, 50] }
  validates :count, numericality: {
    only_integer: true, greater_than_or_equal_to: 0
  }

  scope :total, lambda {
    select('denomination * count as row').sum(&:row)
  }

  def row_total
    denomination * count
  end
end
