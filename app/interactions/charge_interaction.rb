# frozen_string_literal: true

class ChargeInteraction < ActiveInteraction::Base
  hash :denomination do
    integer 1, default: 0
    integer 2, default: 0
    integer 5, default: 0
    integer 10, default: 0
    integer 25, default: 0
    integer 50, default: 0
  end

  def execute
    denomination.each do |k, v|
      next if v.zero?
      record = Banknote.find_or_create_by!(denomination: k)
      record.increment!(:count, v)
    end
    { success: true }
  end
end
