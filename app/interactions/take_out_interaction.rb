# frozen_string_literal: true

class TakeOutInteraction < ActiveInteraction::Base
  integer :amount

  delegate :total, to: Banknote

  DENOMINATIONS = [50, 25, 10, 5, 2, 1].freeze

  def execute
    return { error: 'Insuficcient funds' } if amount > total

    if take_out.zero?
      { success: true }
    else
      { error: 'Sorry, no such banknotes' }
    end
  end

  private

  # rubocop:disable Metrics/MethodLength
  def take_out
    remaining = amount.dup

    Banknote.transaction do
      DENOMINATIONS.each do |number|
        # we are finished
        break if remaining.zero?

        # pass if no such banknotes at file
        record = Banknote.find_by(denomination: number)
        next unless record
        next if record.count.zero?

        # how much we can take
        notes_to_take = [remaining / number, record.count].min
        amount_to_take = notes_to_take * number

        record.decrement!(:count, notes_to_take)
        remaining -= amount_to_take
      end
    end
    remaining
  end
  # rubocop:enable Metrics/MethodLength
end
