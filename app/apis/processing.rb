# frozen_string_literal: true

module ATM
  class Processing < Grape::API
    validation = proc { requires :denomination, type: Hash }

    desc 'charge ATM'
    params(&validation)
    post :charge do
      action = ChargeInteraction.run(params)
      if action.valid?
        action.result
      else
        action.errors.messages
      end
    end

    desc 'take out from ATM'
    params do
      requires :amount, type: Integer
    end
    post :take_out do
      action = TakeOutInteraction.run(params)
      if action.valid?
        action.result
      else
        action.errors.messages
      end
    end
  end
end
