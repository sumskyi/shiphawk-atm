# frozen_string_literal: true

require_relative './processing'

module ATM
  class Base < Grape::API
    format :json
    default_error_status 403
    rescue_from :all

    mount ATM::Processing
  end
end
